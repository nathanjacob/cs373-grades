#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = line-too-long

# ---------
# Grades.py
# ---------

# https://docs.python.org/3.6/library/functools.html
# https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html


# -----------
# grades_eval
# -----------
# each of these return an integer in place for a letter grade for the subsection total
def eval_projects(num):
    # pre: column total
    # post: letter grade for category
    assert num >= 0
    if num >= 5:
        return 11
    if num >= 4:
        return 9
    if num >= 3:
        return 3
    return 0


def eval_exercises(num):
    # pre: column total
    # post: letter grade for category
    assert num >= 0
    if num >= 11:
        return 11
    if num >= 10:
        return 9
    if num >= 9:
        return 6
    if num >= 8:
        return 4
    if num >= 7:
        return 1
    return 0


def eval_blogs(num):
    # pre: column total
    # post: letter grade for category
    assert num >= 0
    retVal = 0
    if num >= 13:
        retVal = 11
    elif num >= 12:
        retVal = 9
    elif num >= 11:
        retVal = 7
    elif num >= 10:
        retVal = 5
    elif num >= 9:
        retVal = 3
    elif num >= 8:
        retVal = 1
    return retVal


def eval_papers(num):
    # pre: column total
    # post: letter grade for category
    assert num >= 0
    retVal = 0
    if num >= 13:
        retVal = 11
    elif num >= 12:
        retVal = 9
    elif num >= 11:
        retVal = 7
    elif num >= 10:
        retVal = 5
    elif num >= 9:
        retVal = 3
    elif num >= 8:
        retVal = 1
    return retVal


def eval_quizzes(num):
    assert num >= 0
    # pre: column total
    # post: letter grade for category
    retVal = 0
    if num >= 39:
        retVal = 11
    elif num >= 38:
        retVal = 10
    elif num >= 37:
        retVal = 9
    elif num >= 35:
        retVal = 8
    elif num >= 34:
        retVal = 7
    elif num >= 32:
        retVal = 6
    elif num >= 31:
        retVal = 5
    elif num >= 29:
        retVal = 4
    elif num >= 28:
        retVal = 3
    elif num >= 27:
        retVal = 2
    elif num >= 25:
        retVal = 1
    return retVal


def grades_eval(l_l_scores: list[list[int]]) -> str:
    # pre: array of individual assignment grades
    # post: string with letter grade
    assert l_l_scores
    length = len(l_l_scores)
    total = [0] * 5
    for i in range(0, length):
        total[i] = l_l_scores[i].count(2) + l_l_scores[i].count(3)
        total[i] += min(l_l_scores[i].count(1), l_l_scores[i].count(3) // 2)
        # ensures that we don't add any more to the total if there are many 3s and few 1s
    # dictionary with numbers and their letter equivalent
    numToGrade = {
        11: "A",
        10: "A-",
        9: "B+",
        8: "B",
        7: "B-",
        6: "C+",
        5: "C",
        4: "C-",
        3: "D+",
        2: "D",
        1: "D-",
        0: "F",
    }

    total[0] = eval_projects(total[0])
    total[1] = eval_exercises(total[1])
    total[2] = eval_blogs(total[2])
    total[3] = eval_papers(total[3])
    total[4] = eval_quizzes(total[4])
    return numToGrade[min(total)]
