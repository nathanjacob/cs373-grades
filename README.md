# CS373: Software Engineering Grades Repo

* Name: Nathan Jacob

* EID: ngj262

* GitLab ID: nathanjacob

* HackerRank ID: nathan_jacob9m

* Git SHA: 479e7bc9b5d2b80d98cf5274762a737b0cfd22dc

* GitLab Pipelines: https://gitlab.com/nathanjacob/cs373-grades/-/pipelines/

* Estimated completion time: 10 hours

* Actual completion time: 5 hours

* Comments: n/a
