#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = line-too-long
# pylint: disable = missing-docstring

# --------------
# test_Grades.py
# --------------

# -------
# imports
# -------

import unittest  # main, TestCase

import Grades

# ----------------
# test_grades_eval
# ----------------


class test_grades_eval(unittest.TestCase):
    def test_0(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 5, [3] * 12, [3] * 14, [3] * 14, [3] * 42]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "A")

    def test_1(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 3, [3] * 9, [3] * 11, [3] * 11, [3] * 38]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D+")

    def test_2(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 10, [3] * 12, [3] * 12, [3] * 37]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "B+")

    def test_3(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 8, [3] * 10, [3] * 10, [3] * 35]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "C-")

    def test_4(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 7, [3] * 9, [3] * 9, [3] * 34]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_5(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 7, [3] * 8, [3] * 8, [3] * 32]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_6(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 7, [3] * 8, [3] * 8, [3] * 31]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_7(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 7, [3] * 8, [3] * 8, [3] * 29]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_8(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 7, [3] * 8, [3] * 8, [3] * 28]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_9(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 7, [3] * 8, [3] * 8, [3] * 27]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_10(self) -> None:
        l_l_scores: list[list[int]] = [[3] * 4, [3] * 7, [3] * 8, [3] * 8, [3] * 25]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "D-")

    def test_11(self) -> None:
        l_l_scores: list[list[int]] = [[0] * 5, [0] * 12, [0] * 14, [0] * 14, [0] * 42]
        letter: str = Grades.grades_eval(l_l_scores)
        self.assertEqual(letter, "F")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
